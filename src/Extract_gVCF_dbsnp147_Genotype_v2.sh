#!/bin/bash
###################################################
# Run as ./Extract_Genotype_Commands.sh $1 $2 $3 $4
#
#
#Input Files: as Command Line Arguments
#
#Command Line Argument 1 ($1 in this script) == gvcf file Location
#Command Line Argument 2 ($2 in this script) == vcf file Location
#Command Line Argument 3 ($3 in this script) == SampleID
#Command Line Argument 4 ($4 in this script) == Path of various script and Data to run this Analysis
#
#
###################################################
#
#
# Assignment
gVCF=$1
VCF=$2
SampleID=$3
MyPath=$4
#
### gVCF File Preprocessing ######################
#
#
LC_ALL=C fgrep -v -w "GT:AD:DP:GQ:PL:SB" $gVCF > $SampleID.NonVariant_Block.gvcf
/opt/gvcftools-0.16/bin/break_blocks --region-file $MyPath/dbsnp147_23nMeID_SNP.bed --exclude-off-target --ref /staging/human/reference/EDGC-hg19_v4/M_hg19_ucsc_all.fa < $SampleID.NonVariant_Block.gvcf > $SampleID.NonVariant_Break_Block.vcf
/opt/bcftools/bcftools annotate -a $MyPath/dbsnp147_23nMeID_SNP.vcf.gz -c CHROM,POS,ID $SampleID.NonVariant_Break_Block.vcf -o $SampleID.NonVariant_Break_Block_WithSNPID.vcf
/opt/vcflib/bin/vcfgenotypes $SampleID.NonVariant_Break_Block_WithSNPID.vcf > $SampleID.NonVariant_Break_Block_WithSNPID_Genotype.vcf
#
####################################################
#
#
#
### VCF File Preprocessing #######################
#
/opt/gvcftools-0.16/bin/break_blocks --region-file $MyPath/dbsnp147_23nMeID_SNP.bed --exclude-off-target --ref $MyPath/M_hg19_ucsc_all.fa < $VCF > $SampleID.Variant_Break_Block.vcf
/opt/bcftools/bcftools annotate -a $MyPath/dbsnp147_23nMeID_SNP.vcf.gz -c CHROM,POS,ID $SampleID.Variant_Break_Block.vcf -o $SampleID.Variant_Break_Block_WithSNPID.vcf
/opt/vcflib/bin/vcfgenotypes $SampleID.Variant_Break_Block_WithSNPID.vcf > $SampleID.Variant_Break_Block_WithSNPID_Genotype.vcf
#
####################################################
#
#
#Remove Lines start with # NOW~~~
#
sed -i '/^#/d' $SampleID.Variant_Break_Block_WithSNPID.vcf
sed -i '/^#/d' $SampleID.NonVariant_Break_Block_WithSNPID.vcf
#
#
#Join GT to SNPID
#
#
join -t $'\t' -j 2 -o 1.3,1.1,1.2,2.6 $SampleID.Variant_Break_Block_WithSNPID.vcf $SampleID.Variant_Break_Block_WithSNPID_Genotype.vcf > $SampleID.Variant.dbsnp147_Output1.txt
join -t $'\t' -j 2 -o 1.3,1.1,1.2,2.6 $SampleID.NonVariant_Break_Block_WithSNPID.vcf $SampleID.NonVariant_Break_Block_WithSNPID_Genotype.vcf > $SampleID.NonVariant.dbsnp147_Output1.txt
#
#
#Remove INDEL step : step1
#
#
awk -v OFS='\t' -F: '{print $1, $2, $3}' $SampleID.Variant.dbsnp147_Output1.txt > $SampleID.Variant.dbsnp147_Output2.txt
awk -v OFS='\t' -F: '{print $1, $2, $3}' $SampleID.NonVariant.dbsnp147_Output1.txt > $SampleID.NonVariant.dbsnp147_Output2.txt
#
#
#Remove INDEL step : step2
#
#
awk -v OFS='\t' '{gsub(/\//, "", $5); print $1,$2, $3, $5} ' $SampleID.Variant.dbsnp147_Output2.txt > $SampleID.Variant.dbsnp147_Output3.txt
awk -v OFS='\t' '{gsub(/\//, "", $5); print $1,$2, $3, $5} ' $SampleID.NonVariant.dbsnp147_Output2.txt > $SampleID.NonVariant.dbsnp147_Output3.txt
#
#
#Remove INDEL step : step3
#
#
awk -v OFS='\t' '{ if (length($4) == 2 ) print $1, $2, $3, $4}' $SampleID.Variant.dbsnp147_Output3.txt > $SampleID.Variant.dbsnp147_Output4.txt
awk -v OFS='\t' '{ if (length($4) == 2 ) print $1, $2, $3, $4}' $SampleID.NonVariant.dbsnp147_Output3.txt > $SampleID.NonVariant.dbsnp147_Output4.txt
#
#
# Join Variant and NonVariant Files
#
#
cat $SampleID.Variant.dbsnp147_Output4.txt $SampleID.NonVariant.dbsnp147_Output4.txt > $SampleID.dbsnp147_Informative_GT.txt
#
#
# Change chrID
#
#
sed -i 's/chrM/MT/g' $SampleID.dbsnp147_Informative_GT.txt
sed -i 's/chr//g' $SampleID.dbsnp147_Informative_GT.txt
#
#
rm $SampleID.NonVariant*
rm $SampleID.Variant*
# Use output file as Input to Sex Detemination and Final Reporting Step
#
###########################################################################################################