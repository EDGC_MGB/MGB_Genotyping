#!/usr/bin/env/perl
#use strict;
#use warnings;
#
##############################################################################################################
##
#This script is developed at EONE-DIAGNOMICS Genome Center (www.edgc.com) to extract the Genotype information from the gvcf file.
#
#
#This (gVCF-to-dbsnp147-GT.pl) script requires Dragen/GATK gVCF file or VCF file (or Both) and sampleID/sampleName and OutDir Location as input
#We recommend to provide both the gvcf and vcf file for better results
#
#Usage Example:#
#Please provide gvcf file (and/or) vcf file, SampleID and Output location as command-line argument
#
#Please provide full path of the gvcf and vcf file
#Also, check the extension of gvcf and vcf file. Programs currently accept only '.gvcf' for genomic variant call file and '.vcf' for variant call file
#
#e.g., perl gVCF-to-dbsnp147-GT.pl /path_to_gvcf/MGB_xxxxxx.gvcf /path_to_vcf/MGB_xxxxxx.vcf MGB_SampleID_xx /Path_to_OutDir/
#e.g., perl gVCF-to-dbsnp147-GT.pl /path_to_vcf/MGB_xxxxxx.vcf MGB_SampleID_xx /Path_to_OutDir/
#
#
#Contact Dr. Amit Goyal at a.goyal@edgc.com for any query.
#
#
#############################################################################################################
use File::Basename;
my $path = "$0";
my($filename, $MyPath, $suffix) = fileparse($path);
$MyPath =~ s/\/$//;



#############################################################################################################
#
#	gVCF-to-dbsnp147-GT.pl script needs the below files during the processing ####
#
#Also, Keep the tabix indexed file "dbsnp147_23nMeID_SNP.vcf.gz.tbi" at the same location with the "dbsnp147_23nMeID_SNP.vcf.gz" file
#
my $Extract_GT_gVCF_sh = "$MyPath/Extract_gVCF_dbsnp147_Genotype_v2.sh";				#### Check the location
my $Extract_GT_VCF_sh = "$MyPath/Extract_VCF_dbsnp147_Genotype_v2.sh";					#### Check the location
$MyPath =~ s/src/data/i;
my $bed_dbsnp147 = "$MyPath/v2/dbsnp147_23nMeID_SNP.bed";								#### Check the location								
my $SNPid_dbsnp147_vcf = "$MyPath/v2/dbsnp147_23nMeID_SNP.vcf.gz";						#### Check the location
#
#
my $break_blocks = "/opt/gvcftools-0.16/bin/break_blocks";					#### Check the location
my $bcftools = "/opt/bcftools/bcftools";									#### Check the location
my $vcfgenotypes = "/opt/vcflib/bin/vcfgenotypes";							#### Check the location
#
#############################################################################################################




#############################################################################################################
#
# Please provide gvcf file, SampleID and Output location as command-line argument
# e.g., perl gVCF-to-23andMe-GT.pl ././MGB_xxxxxx.gvcf MGB_xxxxxx /staging/samples/MGB_WGS/MGB_xxxxxx
#
#
my $arg1= $ARGV[0];
my $arg2= $ARGV[1];
my $arg3= $ARGV[2];
my $arg4= $ARGV[3];

my ($arg1_ext) = $arg1 =~ /(\.[^.]+)$/;
my ($arg2_ext) = $arg2 =~ /(\.[^.]+)$/;


if ($arg1_ext =~ /.gvcf/ && $arg2_ext =~ /.vcf/)
{
	chomp $arg1;
	chomp $arg2;
	chomp $arg3;
	chomp $arg4;

	my $gVCF_File = $arg1;
	my $VCF_File = $arg2;
	my $SampleID = $arg3;
	my $OutDir = $arg4;
	

				print "\nStarting time: \n";
				print `date`;

				`mkdir $OutDir`;
				chdir "/$OutDir";
	
				################# Extract Genotype from using the gVCF and VCF File #############################################
				#
				#
				`sh $Extract_GT_gVCF_sh $gVCF_File $VCF_File $SampleID $MyPath`;
				#
				#
				#It writes the all Loci GT results to the "$SampleID.dbsnp147_Informative_GT.txt" to be processed below. 
				#
				#############################################################################################################


				###### Determine Sex and Remove ChrY SNPs in Female Sample ##################################################
				#
				#This script utilizes the Heterozygosity ratio of ChrX and ChrY to determine the Sample Sex
				#
				#If the Sample is females, it skips the ChrY specific SNP's entries in the Final Report
				#If Male, all the dbsnp loci find in the Sample are reported
				#
				#
				my $chrX_Het_Count = 0;
				my $chrY_Het_Count = 0;
				open (INCB, "$VCF_File") or die;
				#
				# go through the file line by line    
				while (my $line = <INCB>)
				{
					chomp ($line);
					chop($line) if ($line =~ m/\r$/);
					# split the current line on tabs
					my @columns = split(/\t/, $line);
					chomp $columns[0];
					chomp $columns[9];

					if ($columns[0] =~ m/chrX/ && $columns[9] !~ m/(^0\/0|^1\/1|^2\/2|^3\/3|^4\/4|^5\/5|^6\/6)/)
						{
						$chrX_Het_Count+=1;
						}

					if ($columns[0] =~ m/chrY/ && $columns[9] !~ m/(^0\/0|^1\/1|^2\/2|^3\/3|^4\/4|^5\/5|^6\/6)/)
						{
						$chrY_Het_Count+=1;
						}
				}

				my $Heterozygosity_ratio = $chrX_Het_Count/$chrY_Het_Count;
				print "\nHeterozygosity ratio: $Heterozygosity_ratio\n";
				#
				# close the filehandle and exit
				#
				close INCB;
				#
				#
				# Determine Sex
				#
				if ($Heterozygosity_ratio >= 1)
				{
				#
				print "\Sample's Gender is: Female \n";
				print "\Sex Detemination done at: ";
				print `date`;
				#
				####Female Sample Processing
				#
				# Sort the report FIle on ChrID and Position
				#
				`grep -v 'X' $OutDir/$SampleID.dbsnp147_Informative_GT.txt | grep -v 'Y' | grep -v 'MT' > $OutDir/$SampleID.gVCF_dbsnp147_GT_Report.txt`;
				`grep 'X' $OutDir/$SampleID.dbsnp147_Informative_GT.txt >> $OutDir/$SampleID.gVCF_dbsnp147_GT_Report.txt`;
				`grep 'MT' $OutDir/$SampleID.dbsnp147_Informative_GT.txt >> $OutDir/$SampleID.gVCF_dbsnp147_GT_Report.txt`;
				#
				#
				`gzip $OutDir/$SampleID.gVCF_dbsnp147_GT_Report.txt`;
				`rm -f $OutDir/$SampleID.dbsnp147_Informative_GT.txt`;
				print "\nFinal Report Made at: ";
				print `date`;
				print "\n";
				#
				######################################################
				}
				else
				{
				#
				print "\Sample's Gender is: Male \n";
				print "\Sex Detemination done at: ";
				print `date`;
				#
				##### Male Sample Processing
				#
				# Sort the report FIle on ChrID and Position
				#
				`grep -v 'X' $OutDir/$SampleID.dbsnp147_Informative_GT.txt | grep -v 'Y' | grep -v 'MT' > $OutDir/$SampleID.gVCF_dbsnp147_GT_Report.txt`;
				`grep 'X' $OutDir/$SampleID.dbsnp147_Informative_GT.txt >> $OutDir/$SampleID.gVCF_dbsnp147_GT_Report.txt`;
				`grep 'Y' $OutDir/$SampleID.dbsnp147_Informative_GT.txt >> $OutDir/$SampleID.gVCF_dbsnp147_GT_Report.txt`;
				`grep 'MT' $OutDir/$SampleID.dbsnp147_Informative_GT.txt >> $OutDir/$SampleID.gVCF_dbsnp147_GT_Report.txt`;
				#
				#
				`gzip $OutDir/$SampleID.gVCF_dbsnp147_GT_Report.txt`;
				`rm -f $OutDir/$SampleID.dbsnp147_Informative_GT.txt`;
				print "\nFinal Report Made at: ";
				print `date`;
				print "\n";
				}
				#
				#############################################################################################################
}

elsif($arg1_ext =~ /.vcf/)
{
chomp $arg1;
chomp $arg2;
chomp $arg3;

my $VCF_File = $arg1;
my $SampleID = $arg2;
my $OutDir = $arg3;	
	
	
				print "\nStarting time: \n";
				print `date`;

				`mkdir $OutDir`;
				chdir "/$OutDir";
	
				################# Extract Genotype from using the gVCF and VCF File #############################################
				#
				#
				`sh $Extract_GT_VCF_sh $VCF_File $SampleID $MyPath`;
				#
				#
				#It writes the all Loci GT results to the "$SampleID.dbsnp147_Informative_GT.txt" to be processed below. 
				#
				#############################################################################################################


				###### Determine Sex and Remove ChrY SNPs in Female Sample ##################################################
				#
				#This script utilizes the Heterozygosity ratio of ChrX and ChrY to determine the Sample Sex
				#
				#If the Sample is females, it skips the ChrY specific SNP's entries in the Final Report
				#If Male, all the dbsnp loci find in the Sample are reported
				#
				#
				my $chrX_Het_Count = 0;
				my $chrY_Het_Count = 0;
				open (INCB, "$VCF_File") or die;
				#
				# go through the file line by line    
				while (my $line = <INCB>)
				{
					chomp ($line);
					chop($line) if ($line =~ m/\r$/);
					# split the current line on tabs
					my @columns = split(/\t/, $line);
					chomp $columns[0];
					chomp $columns[9];

					if ($columns[0] =~ m/chrX/ && $columns[9] !~ m/(^0\/0|^1\/1|^2\/2|^3\/3|^4\/4|^5\/5|^6\/6)/)
						{
						$chrX_Het_Count+=1;
						}

					if ($columns[0] =~ m/chrY/ && $columns[9] !~ m/(^0\/0|^1\/1|^2\/2|^3\/3|^4\/4|^5\/5|^6\/6)/)
						{
						$chrY_Het_Count+=1;
						}
				}

				my $Heterozygosity_ratio = $chrX_Het_Count/$chrY_Het_Count;
				print "\nHeterozygosity ratio: $Heterozygosity_ratio\n";
				#
				# close the filehandle and exit
				#
				close INCB;
				#
				#
				# Determine Sex
				#
				if ($Heterozygosity_ratio >= 1)
				{
				#
				print "\Sample's Gender is: Female \n";
				print "\Sex Detemination done at: ";
				print `date`;
				#
				####Female Sample Processing
				#
				# Sort the report FIle on ChrID and Position
				#
				`grep -v 'X' $OutDir/$SampleID.dbsnp147_Informative_GT.txt | grep -v 'Y' | grep -v 'MT' > $OutDir/$SampleID.VCF_dbsnp147_GT_Report.txt`;
				`grep 'X' $OutDir/$SampleID.dbsnp147_Informative_GT.txt >> $OutDir/$SampleID.VCF_dbsnp147_GT_Report.txt`;
				`grep 'MT' $OutDir/$SampleID.dbsnp147_Informative_GT.txt >> $OutDir/$SampleID.VCF_dbsnp147_GT_Report.txt`;
				#
				#
				`gzip $OutDir/$SampleID.VCF_dbsnp147_GT_Report.txt`;
				`rm -f $OutDir/$SampleID.dbsnp147_Informative_GT.txt`;
				print "\nFinal Report Made at: ";
				print `date`;
				print "\n";
				#
				######################################################
				}
				else
				{
				#
				print "\Sample's Gender is: Male \n";
				print "\Sex Detemination done at: ";
				print `date`;
				#
				##### Male Sample Processing
				#
				# Sort the report FIle on ChrID and Position
				#
				`grep -v 'X' $OutDir/$SampleID.dbsnp147_Informative_GT.txt | grep -v 'Y' | grep -v 'MT' > $OutDir/$SampleID.VCF_dbsnp147_GT_Report.txt`;
				`grep 'X' $OutDir/$SampleID.dbsnp147_Informative_GT.txt >> $OutDir/$SampleID.VCF_dbsnp147_GT_Report.txt`;
				`grep 'Y' $OutDir/$SampleID.dbsnp147_Informative_GT.txt >> $OutDir/$SampleID.VCF_dbsnp147_GT_Report.txt`;
				`grep 'MT' $OutDir/$SampleID.dbsnp147_Informative_GT.txt >> $OutDir/$SampleID.VCF_dbsnp147_GT_Report.txt`;
				#
				#
				`gzip $OutDir/$SampleID.VCF_dbsnp147_GT_Report.txt`;
				`rm -f $OutDir/$SampleID.dbsnp147_Informative_GT.txt`;
				print "\nFinal Report Made at: ";
				print `date`;
				print "\n";
				}
				#
				#############################################################################################################

}