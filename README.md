This script is generated to convert the vcf/gVCF file to the 23andMe like Genotyping script for MyGenomeBox Corp.  
Contact: Dr. Amit Goyal ## a.goyal@edgc.com || www.edgc.com  


# MGB_Genotyping_v4.1 --> Script: VariantFile-to-dbsnp147-GT_v4.pl  
What's New:  
1. Minor bug fix relatd to the location of dependent database (/data/v4/dbsnp147_plus.bed, /data/v4/dbsnp147_plus.vcf.gz) and Refernce file)



# MGB_Genotyping_v4 --> Script: VariantFile-to-dbsnp147-GT_v4.pl  
What's New:  
1. Solved VCF missing Header Issue. This script will add/modify the vcf/gvcf header if is absent/malformed in input files.  

### How to Run:
	perl /Path/to/VariantFile-to-dbsnp147-GT_v4.pl /Path/Input.gvcf /Path/Input.vcf SampleID /Path/to/OutDir  
	or  
	perl /Path/to/VariantFile-to-dbsnp147-GT_v4.pl /Path/Input.vcf SampleID /Path/to/OutDir  
	or  
	perl /Path/to/VariantFile-to-dbsnp147-GT_v4.pl /Path/Input.gvcf SampleID /Path/to/OutDir  

#### Dependency: Install the dependency software and check the location in various (.pl and/or .sh) files.
    Tools:			
    gvcftools --> gvcftools-0.16.tar.gz
    bcftools --> bcftools-1.2.zip
    vcflib --> vcflib-master.zip
    Files:
    Targetted Sites --> ~/data/v4/dbsnp147_plus.bed
    Ref_GT_File --> ~/data/v4/dbsnp147_plus.vcf.gz
	vcf/gvcf Header --> ~/data/v4/VCF_Header_v4.txt & ~/data/v4/gVCF_Header_v4.txt
    Please check the Dependency File for the Set_up file and reference data. 


# MGB_Genotyping_v3 --> Script: VariantFile-to-dbsnp147-GT_v3.pl  
What's New:  
1. Updated INDEL sites in dbsnp147 build. (however GT containing INDEL are not reported in final GT)  

### How to Run:
	perl /Path/to/VariantFile-to-dbsnp147-GT_v3.pl /Path/Input.gvcf /Path/Input.vcf SampleID /Path/to/OutDir  
	or  
	perl /Path/to/VariantFile-to-dbsnp147-GT_v3.pl /Path/Input.vcf SampleID /Path/to/OutDir  
	or  
	perl /Path/to/VariantFile-to-dbsnp147-GT_v3.pl /Path/Input.gvcf SampleID /Path/to/OutDir  

### Improvement:  
	More Number of Markers (INDEL site) and faster.  

#### Dependency: Install the dependency software and check the location in various (.pl and/or .sh) files.
    Tools:			
    gvcftools --> gvcftools-0.16.tar.gz
    bcftools --> bcftools-1.2.zip
    vcflib --> vcflib-master.zip
    Files:
    Targetted Sites --> ~/data/v3/dbsnp147_plus.bed
    Ref_GT_File --> ~/data/v3/dbsnp147_plus.vcf.gz
    Please check the Dependency File for the Set_up file and reference data.  


# MGB_Genotyping_v2 --> Script: VariantFile-to-dbsnp147-GT_v2.pl  
What's New:  
1. Updated Markers to list all the 23andMe markers, KIRAgen markers and all SNP markers in dbsnp147 build.  
2. Just a single script file which identifiy if input is vcf/gvcf or both and process the data accordingly.  

### How to Run:
	perl /Path/to/VariantFile-to-dbsnp147-GT_v2.pl /Path/Input.gvcf /Path/Input.vcf SampleID /Path/to/OutDir  
	or  
	perl /Path/to/VariantFile-to-dbsnp147-GT_v2.pl /Path/Input.vcf SampleID /Path/to/OutDir  
	or  
	perl /Path/to/VariantFile-to-dbsnp147-GT_v2.pl /Path/Input.gvcf SampleID /Path/to/OutDir  

### Improvement:  
	More Number of Markers and easy to use.  

#### Dependency: Install the dependency software and check the location in various (.pl and/or .sh) files.
    Tools:			
    gvcftools --> gvcftools-0.16.tar.gz
    bcftools --> bcftools-1.2.zip
    vcflib --> vcflib-master.zip
    Files:
    Targetted Sites --> ~/data/v2/dbsnp147_23nMeID_SNP.bed
    Ref_GT_File --> ~/data/v2/dbsnp147_23nMeID_SNP.vcf.gz
    Please check the Dependency File for the Set_up file and reference data.  



# MGB_Genotyping_v1 --> Script: VCF-to-23andMe-GT_v1.pl and gVCF-to-23andMe-GT_v1.pl  
This script accepts the given vcf/gvcf file as input and generates the Getotyping information for the Markers listed in v1 databse.  

### How to Run:
	perl /Path/VCF-to-23andMe-GT.pl /Path/Input.vcf SampleID /Path/to/OutDir  
	or  
	perl /Path/gVCF-to-23andMe-GT.pl /Path/Input.gvcf /Path/Input.vcf SampleID /Path/to/OutDir  

#### Note
    Note 1. Keep the directory structure intact or Check the dependency files location.  
  
#### Dependency: Install the dependency software and check the location in various (.pl and/or .sh) files.
    Tools:			
    gvcftools --> gvcftools-0.16.tar.gz
    bcftools --> bcftools-1.2.zip
    vcflib --> vcflib-master.zip
    Files:
    Targetted Sites --> ~/data/v1/23nMe_Genotype.bed
    Ref_GT_File --> ~/data/v1/23andMe.vcf*
    Sex_Determize_File --> ~/data/v1/23andMe_XX.vcf & ~/data/v1/23andMe_XY.vcf
    Please check the Dependency File for the Set_up file and reference data.