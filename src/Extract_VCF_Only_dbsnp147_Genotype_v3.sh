#!/bin/bash
#############################################################################################################
#
## Input Files: as Command Line Arguments from the previos script (Variants-to-dbsnp147-GT.pl).
#
#Command Line Argument 1 ($VCF in this script) == vcf file Location
#Command Line Argument 2 ($SampleID in this script) == SampleID
#Command Line Argument 3 ($MyPath in this script) == Path of main script (Variants-to-dbsnp147-GT.pl) and Reference Data to run this Analysis
#
#############################################################################################################
#############################################################################################################
#
## This script requires the below mentioned programs.
#
#Please download these programs at appropriate location or modify the program directory here (when needed)
#gvcftools = version: 0.16
#bcftools = bcftools 1.2-187-g1a55e45
#vcflib = No Specific Version information available, Please use the to download the software build "git clone --recursive https://github.com/ekg/vcflib.git"
#
#
gvcftools=/opt/gvcftools-0.16							#### Check the location
bcftools=/opt/bcftools/bcftools							#### Check the location
vcfgenotypes=/opt/vcflib/bin/vcfgenotypes				#### Check the location
#
#############################################################################################################
#############################################################################################################


#############################################################################################################
#############################################################################################################
## Processing INPUT Command Line arguments
VCF=$1
SampleID=$2
MyPath=$3
#
#
## VCF File Preprocessing #######################
#
#
$gvcftools/bin/break_blocks --region-file $MyPath/dbsnp147_plus.bed --exclude-off-target --ref $MyPath//M_hg19_ucsc_all.fa < $VCF > $SampleID.Variant.Break_Block.vcf
$bcftools annotate -a $MyPath/dbsnp147_plus.vcf.gz -c CHROM,POS,ID $SampleID.Variant.Break_Block.vcf -o $SampleID.Variant.Break_Block.Annotated.vcf
$vcfgenotypes $SampleID.Variant.Break_Block.Annotated.vcf > $SampleID.Variant.Break_Block.Annotated.Genotype.vcf
#
#
## Genotype Reformatting Steps ##################
#
awk -v OFS='\t' -F":" '{print $1, $2, $3}' $SampleID.Variant.Break_Block.Annotated.Genotype.vcf > $SampleID.Variant.Break_Block.Annotated.Genotype2.vcf
sed -i '/^#/d' $SampleID.Variant.Break_Block.Annotated.vcf
join -t $'\t' -1 2 -2 2 -o 1.1,1.2,1.3,1.4,2.7 $SampleID.Variant.Break_Block.Annotated.vcf $SampleID.Variant.Break_Block.Annotated.Genotype2.vcf > $SampleID.Variant.Break_Block.dbsnp147_plus_Output1.txt
awk -v OFS='\t' '{print $3, $1, $2, $5}' $SampleID.Variant.Break_Block.dbsnp147_plus_Output1.txt > $SampleID.dbsnp147_plus_Informative_GT1.txt
#
#
## Change chrID and sort again ##################
#
#
sed -i 's/chrM/MT/g' $SampleID.dbsnp147_plus_Informative_GT1.txt
sed -i 's/chr//g' $SampleID.dbsnp147_plus_Informative_GT1.txt
LC_ALL=C sort -k2,2n -k3,3n $SampleID.dbsnp147_plus_Informative_GT1.txt > $SampleID.dbsnp147_plus_Informative_GT2.txt
awk -v OFS='\t' '{print $1, $2, $3, toupper($4)}' $SampleID.dbsnp147_plus_Informative_GT2.txt > $SampleID.dbsnp147_plus_Informative_GT.txt
#
#
rm $SampleID.dbsnp147_plus_Informative_GT1.txt
rm $SampleID.dbsnp147_plus_Informative_GT2.txt
rm $SampleID.Variant*
# Use output file as Input to Sex Detemination and Final Reporting Step
#
#############################################################################################################
#############################################################################################################