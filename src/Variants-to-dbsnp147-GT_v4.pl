#!/usr/bin/env/perl
#use strict;
#use warnings;
#
#############################################################################################################
##
#This script is developed at EONE-DIAGNOMICS Genome Center (www.edgc.com) to extract the Genotype information from the gvcf file.
#This (Variants-to-dbsnp147-GT.pl) script requires Dragen/GATK VCF, gVCF file, sampleID/sampleName and OutDir Location as input
#
#############################################################################################################
#############################################################################################################
#Usage Example:#
# e.g., perl /Path_to_script/Variants-to-dbsnp147-GT.pl ././MGB_xxxxxx.vcf MGB_xxxxxx /staging/samples/MGB_WGS/MGB_xxxxxx
# e.g., perl /Path_to_script/Variants-to-dbsnp147-GT.pl ././MGB_xxxxxx.gvcf MGB_xxxxxx /staging/samples/MGB_WGS/MGB_xxxxxx
# e.g., perl /Path_to_script/Variants-to-dbsnp147-GT.pl ././MGB_xxxxxx.vcf ././MGB_xxxxxx.gvcf MGB_xxxxxx /staging/samples/MGB_WGS/MGB_xxxxxx
#
#############################################################################################################
#############################################################################################################
#Please provide full path of the perl script as well as VCF and gvcf files.
#Contact Dr. Amit Goyal at a.goyal@edgc.com for any query.
#
#############################################################################################################
#############################################################################################################
#
#
use File::Basename;
my $path = "$0";
my($filename, $MyPath, $suffix) = fileparse($path);
$MyPath =~ s/\/$//;
#
#
#############################################################################################################
#
## Variants-to-dbsnp147-GT.pl script needs the below files during the processing ####
## Also, Keep the tabix indexed file "dbsnp147.vcf.gz.tbi" at the same location with the "dbsnp147.vcf.gz" file
#
my $Extract_GT_gVCF_sh = "$MyPath/Extract_gVCF_Only_dbsnp147_Genotype_v4.sh";			#### Check the location
my $Extract_GT_VCF_sh = "$MyPath/Extract_VCF_Only_dbsnp147_Genotype_v4.sh";				#### Check the location
my $Extract_GT_VCF_gVCF_sh = "$MyPath/Extract_VCF_gVCF_dbsnp147_Genotype_v4.sh";		#### Check the location
my $Process_chrY = "$MyPath/Process_chrY_v4.pl";										#### Check the location
my $vcfHeader = "$MyPath/VCF_Header_v4.txt";											#### Check the location
my $gvcfHeader = "$MyPath/gVCF_Header_v4.txt";											#### Check the location
#
$MyPath =~ s/src$/data/i;
my $bed_dbsnp147 = "$MyPath/v4/dbsnp147_plus.bed";										#### Check the location								
my $SNPid_dbsnp147_vcf = "$MyPath/v4/dbsnp147_plus.vcf.gz";								#### Check the location
#
##############################################################################################################
#
## Processing INPUT Command Line arguments
#
my $arg1= $ARGV[0]; chomp $arg1;
my $arg2= $ARGV[1]; chomp $arg2;
my $arg3= $ARGV[2]; chomp $arg3;
my $arg4= $ARGV[3]; chomp $arg4;
#
#############################################################################################################
#############################################################################################################


#############################################################################################################
#############################################################################################################
# (1) quit unless we have the correct number of command-line args

$num_args = $#ARGV + 1;
if ($num_args < 3)
{
	print "\nERROR: Required Minimum 3 Arguments \n\n";
	print "\nVariant File to Genotyping Script: This script is developed to convert the variants in the VCF/gVCF (or both) Files to the 23andMe like Genotyping Table.";
    print "\nUsage Examples:";
	print "\nperl /Path_to_script/Variants-to-dbsnp147-GT.pl SampleID.vcf SampleID.gvcf SampleID path_to_OutputDir";
	print "\nOR";
	print "\nperl /Path_to_script/Variants-to-dbsnp147-GT.pl SampleID.gvcf SampleID path_to_OutputDir";
	print "\nOR";
	print "\nperl /Path_to_script/Variants-to-dbsnp147-GT.pl SampleID.vcf SampleID path_to_OutputDir\n\n";
    exit;
}
elsif($arg1 eq "-h" || $arg1 eq "-help" || $arg1 eq "--help")
{
	print "\nVariant File to Genotyping Script: This script is developed to convert the variants in the VCF/gVCF (or both) Files to the 23andMe like Genotyping Table.";
    print "\nThis script requires minimum 3 arguments.\n\n";
	print "Usage Examples:";
	print "\nperl /Path_to_script/Variants-to-dbsnp147-GT.pl SampleID.vcf SampleID.gvcf SampleID path_to_OutputDir";
	print "\nOR";
	print "\nperl /Path_to_script/Variants-to-dbsnp147-GT.pl SampleID.gvcf SampleID path_to_OutputDir";
	print "\nOR";
	print "\nperl /Path_to_script/Variants-to-dbsnp147-GT.pl SampleID.vcf SampleID path_to_OutputDir\n\n";
    exit;
}
#############################################################################################################
#############################################################################################################


#############################################################################################################
#############################################################################################################
if ($num_args < 4)
{
	my ($arg1_ext) = $arg1 =~ /(\.[^.]+)$/;
	if ($arg1_ext eq ".gvcf")
	{
		my $gVCF_File = $arg1;
		my $SampleID = $arg2;
		my $OutDir = $arg3;
		
		`mkdir $OutDir/$SampleID`;
		chdir "/$OutDir/$SampleID";
		
		print "\nStarting time: ";
		print `date`;
		
		my $gvcfCount=`grep -c "^#" $gVCF_File`;
		if ($gvcfCount<100)
		{
			print "gVCF Header is Marformed, Reformating gVCF file.\n\n";
			`grep -v "^#" $gVCF_File > $OutDir/$SampleID/$SampleID.NoHeader.gvcf`;
			`cat $gvcfHeader $OutDir/$SampleID/$SampleID.NoHeader.gvcf > $OutDir/$SampleID/$SampleID.WithHeader.gvcf`;
			`sh $Extract_GT_gVCF_sh $OutDir/$SampleID/$SampleID.WithHeader.gvcf $SampleID $MyPath`;
		}
		else
		{
			`sh $Extract_GT_gVCF_sh $gVCF_File $SampleID $MyPath`;
		}
		#
		`perl $Process_chrY $OutDir/$SampleID/$SampleID.Variant.vcf $SampleID $OutDir`;
		#
	}
	
	elsif($arg1_ext eq ".vcf")
	{
		my $VCF_File = $arg1;
		my $SampleID = $arg2;
		my $OutDir = $arg3;
		
		`mkdir $OutDir/$SampleID`;
		chdir "/$OutDir/$SampleID";
		
		print "\nStarting time: ";
		print `date`;
		
		my $vcfCount=`grep -c "^#" $VCF_File`;
		if ($vcfCount<100)
		{
			print "VCF Header is Malformed, Reformating VCF file.\n\n";
			`grep -v "^#" $VCF_File > $OutDir/$SampleID/$SampleID.NoHeader.vcf`;
			`cat $vcfHeader $OutDir/$SampleID/$SampleID.NoHeader.vcf > $OutDir/$SampleID/$SampleID.WithHeader.vcf`;
			#
			`sh $Extract_GT_VCF_sh $OutDir/$SampleID/$SampleID.WithHeader.vcf $SampleID $MyPath`;
			`perl $Process_chrY $OutDir/$SampleID/$SampleID.WithHeader.vcf $SampleID $OutDir`;
		}
		else
		{
			`sh $Extract_GT_VCF_sh $VCF_File $SampleID $MyPath`;
			`perl $Process_chrY $VCF_File $SampleID $OutDir`;
		}
	}
	else
	{
		print "\nERROR: CHECK Input Arguments \n\n";
		print "\nVariant File to Genotyping Script: This script is developed to convert the variants in the VCF/gVCF (or both) Files to the 23andMe like Genotyping Table.";
		print "\nUsage Examples:";
		print "\nperl /Path_to_script/Variants-to-dbsnp147-GT.pl SampleID.vcf SampleID.gvcf SampleID path_to_OutputDir";
		print "\nOR";
		print "\nperl /Path_to_script/Variants-to-dbsnp147-GT.pl SampleID.gvcf SampleID path_to_OutputDir";
		print "\nOR";
		print "\nperl /Path_to_script/Variants-to-dbsnp147-GT.pl SampleID.vcf SampleID path_to_OutputDir\n\n";
		exit;
	}
}
#############################################################################################################
#############################################################################################################


#############################################################################################################
#############################################################################################################
if ($num_args == 4)
{
	my ($arg1_ext) = $arg1 =~ /(\.[^.]+)$/;
	my ($arg2_ext) = $arg2 =~ /(\.[^.]+)$/;
	
	if ($arg1_ext =~ /.vcf/ && $arg2_ext =~ /.gvcf/)
	{
		my $VCF_File = $arg1;
		my $gVCF_File = $arg2;
		my $SampleID = $arg3;
		my $OutDir = $arg4;
		
		`mkdir $OutDir/$SampleID`;
		chdir "/$OutDir/$SampleID";

		print "\nStarting time: ";
		print `date`;

		my $vcfCount=`grep -c "^#" $VCF_File`;
		my $gvcfCount=`grep -c "^#" $gVCF_File`;
		if ($vcfCount<100 || $gvcfCount<100)
		{
			print "VCF/gVCF Header is Marformed, Reformating VCF/gVCF file.\n\n";
			`grep -v "^#" $VCF_File > $OutDir/$SampleID/$SampleID.NoHeader.vcf`;
			`cat $vcfHeader $OutDir/$SampleID/$SampleID.NoHeader.vcf > $OutDir/$SampleID/$SampleID.WithHeader.vcf`;
			`grep -v "^#" $gVCF_File > $OutDir/$SampleID/$SampleID.NoHeader.gvcf`;
			`cat $gvcfHeader $OutDir/$SampleID/$SampleID.NoHeader.gvcf > $OutDir/$SampleID/$SampleID.WithHeader.gvcf`;
			#
			`sh $Extract_GT_VCF_gVCF_sh $OutDir/$SampleID/$SampleID.WithHeader.vcf $OutDir/$SampleID/$SampleID.WithHeader.gvcf $SampleID $MyPath`;
			`perl $Process_chrY $OutDir/$SampleID/$SampleID.WithHeader.vcf $SampleID $OutDir`;
		}
		else
		{
			`sh $Extract_GT_VCF_gVCF_sh $VCF_File $gVCF_File $SampleID $MyPath`;
			`perl $Process_chrY $VCF_File $SampleID $OutDir`;
		}
	}
	
	elsif ($arg1_ext =~ /.gvcf/ && $arg2_ext =~ /.vcf/)
	{
		my $VCF_File = $arg2;
		my $gVCF_File = $arg1;
		my $SampleID = $arg3;
		my $OutDir = $arg4;
		
		`mkdir $OutDir/$SampleID`;
		chdir "/$OutDir/$SampleID";

		print "\nStarting time: ";
		print `date`;
				
		my $vcfCount=`grep -c "^#" $VCF_File`;
		my $gvcfCount=`grep -c "^#" $gVCF_File`;
		if ($vcfCount<100 || $gvcfCount<100)
		{
			print "VCF/gVCF Header is Marformed, Reformating VCF/gVCF file.\n\n";
			`grep -v "^#" $VCF_File > $OutDir/$SampleID/$SampleID.NoHeader.vcf`;
			`cat $vcfHeader $OutDir/$SampleID/$SampleID.NoHeader.vcf > $OutDir/$SampleID/$SampleID.WithHeader.vcf`;
			`grep -v "^#" $gVCF_File > $OutDir/$SampleID/$SampleID.NoHeader.gvcf`;
			`cat $gvcfHeader $OutDir/$SampleID/$SampleID.NoHeader.gvcf > $OutDir/$SampleID/$SampleID.WithHeader.gvcf`;
			#
			`sh $Extract_GT_VCF_gVCF_sh $OutDir/$SampleID/$SampleID.WithHeader.vcf $OutDir/$SampleID/$SampleID.WithHeader.gvcf $SampleID $MyPath`;
			`perl $Process_chrY $OutDir/$SampleID/$SampleID.WithHeader.vcf $SampleID $OutDir`;
		}
		else
		{
			`sh $Extract_GT_VCF_gVCF_sh $VCF_File $gVCF_File $SampleID $MyPath`;
			`perl $Process_chrY $VCF_File $SampleID $OutDir`;
		}
	}
	else
	{
		print "\nERROR: Check Input Arguments \n\n";
		print "\nVariant File to Genotyping Script: This script is developed to convert the variants in the VCF/gVCF (or both) Files to the 23andMe like Genotyping Table.";
		print "\nUsage Examples:";
		print "\nperl /Path_to_script/Variants-to-dbsnp147-GT.pl SampleID.vcf SampleID.gvcf SampleID path_to_OutputDir";
		print "\nOR";
		print "\nperl /Path_to_script/Variants-to-dbsnp147-GT.pl SampleID.gvcf SampleID path_to_OutputDir";
		print "\nOR";
		print "\nperl /Path_to_script/Variants-to-dbsnp147-GT.pl SampleID.vcf SampleID path_to_OutputDir\n\n";
		exit;
	}
}
#############################################################################################################
		print "\nFinished time: ";
		print `date`;