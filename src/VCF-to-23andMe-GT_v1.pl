#!/usr/bin/env/perl

#use strict;
#use warnings;

#############################################################################################################
#
#This script is developed at EONE-DIAGNOMICS Genome Center (www.edgc.com) to extract the Genotype information from the gvcf file.
#
#
#This (VCF-to-23andMe-GT.pl) script requires Dragen/GATK VCF file and sampleID/sampleName as input
#
#
#Usage Example:
#Please provide vcf file, SampleID and Output location as command-line argument
#
#e.g., perl VCF-to-23andMe-GT.pl ././MGB_xxxxxx.vcf MGB_xxxxxx /staging/samples/MGB_WGS/MGB_xxxxxx
#
#
#
#Contact Amit Goyal at a.goyal@edgc.com for any query.
#
#
#############################################################################################################

use File::Basename;
my $path = "$0";
my($filename, $MyPath, $suffix) = fileparse($path);
$MyPath =~ s/\/$//;
$MyPath =~ s/src/data/i;

####################################################################
#
my $bed_23andMe = "$MyPath/v1/23nMe_Genotype.bed";											
my $SNPid_23andMe_vcf = "$MyPath/v1/23andMe.vcf.gz";
my $input23andMe_vcf = "$MyPath/v1/23andMe.vcf";		
my $input23andMe_XX_vcf = "$MyPath/v1/23andMe_XX.vcf";
my $input23andMe_XY_vcf = "$MyPath/v1/23andMe_XY.vcf";										  
my $RefGen = "$MyPath/M_hg19_ucsc_all.fa";   #### Check the location
#
#my $break_blocks = "/opt/gvcftools-0.16/bin/break_blocks";	   #### Check the location
my $JAVA = "/opt/jdk1.8.0_91/bin/java";							### Check the location
my $bcftools = "/opt/bcftools/bcftools";					   #### Check the location
my $vcfgenotypes = "/opt/vcflib/bin/vcfgenotypes";             #### Check the location
my $GATK = "/opt/GATK3.3/GenomeAnalysisTK.jar";	           	   #### Check the location
#
####################################################################


####################################################################
#
# Provide SampleID as command-line argument
# e.g., perl gVCF-to-23andMe-GT.pl MIKE.vcf MIKE /staging/samples/MGB_WGS/MGC_MIKE
#
#
my $VCF_File= $ARGV[0];          ##### Provide SampleID.vcf file location for Gender detection
chomp $VCF_File;
my $SampleID = $ARGV[1];          ##### Uniq SampleID ###
chomp $SampleID;
my $OutDir = $ARGV[2];       ####### Location to save the Final 23andMe Report
chomp $OutDir;
#
#
####################################################################
#
####################################################################


print "\nStarting time: ";
print `date`;

`mkdir $OutDir/$SampleID`;





########### VCF file to get VCFto23andMe.vcf#############
#
`$JAVA -jar $GATK -T SelectVariants -R $RefGen -L $bed_23andMe --variant $VCF_File -o $OutDir/$SampleID/$SampleID.VCFto23andMe.vcf`;
#
####################################################################
print "\nSelectVariant at: ";
print `date`;





###########bcftools annotate to add SNPid to the VCFto23andMe.vcf #######
#
`$bcftools annotate -a $SNPid_23andMe_vcf -c CHROM,POS,ID $OutDir/$SampleID/$SampleID.VCFto23andMe.vcf -o $OutDir/$SampleID/$SampleID.23andMe_WithSnpID.vcf`;
`grep -v "#" $OutDir/$SampleID/$SampleID.23andMe_WithSnpID.vcf > $OutDir/$SampleID/$SampleID.23andMe_SnpID.vcf`;
#
`rm -f $OutDir/$SampleID/$SampleID.23andMe_WithSnpID.vcf`;
#
####################################################################





###########vcflib vcfgenotypes to extract GT from VCFto23andMe.vcf ######
#
`$vcfgenotypes $OutDir/$SampleID/$SampleID.VCFto23andMe.vcf > $OutDir/$SampleID/$SampleID.VCFto23andMe_GenoType.txt`;
#
###################################################################
#`rm -f $OutDir/$SampleID/$SampleID.VCFto23andMe.vcf`;

print "\nvcfgenotype Done at: ";
print `date`;





################# Join the Two Files (SNPid and GT)##################
#
`join -j 2 -o 1.3,1.1,1.2,2.6 $OutDir/$SampleID/$SampleID.23andMe_SnpID.vcf $OutDir/$SampleID/$SampleID.VCFto23andMe_GenoType.txt > $OutDir/$SampleID/$SampleID.23andMePreOutput1.txt`;
`perl -p -e 's/ /\t/g' $OutDir/$SampleID/$SampleID.23andMePreOutput1.txt > $OutDir/$SampleID/$SampleID.23andMePreOutput2.txt`;
#
`rm -f $OutDir/$SampleID/$SampleID.23andMePreOutput1.txt`;
`rm -f $OutDir/$SampleID/$SampleID.23andMe_SnpID.vcf`;
#`rm -f $OutDir/$SampleID/$SampleID.VCFto23andMe_GenoType.txt`;
#
####################################################################

print "\nGT and SNP File Combine step Done at: ";
print `date`;





################# Delete INDEL from the 23andMePreOutput2 File #############################################
#
open (IN, "$OutDir/$SampleID/$SampleID.23andMePreOutput2.txt") or die;
open my $OFILE, '>', "$OutDir/$SampleID/$SampleID".'.23andMePreOutput3.txt' or die "Cannot create file for output: $!";

# go through the file line by line    
while (my $line = <IN>)  {

    # split the current line on tabs
    my @columns = split(/\t/, $line);
	
	chomp $columns[0];
	chomp $columns[1];
	chomp $columns[2];
	chomp $columns[3];	
	
	my $GT = $columns[3];
	$GT2 = substr($GT, -3);


	print $OFILE "$columns[0]\t$columns[1]\t$columns[2]\t$GT2\n";
	
}
# close the filehandle and exit
close $OFILE;
close IN;

`rm -f $OutDir/$SampleID/$SampleID.23andMePreOutput2.txt`;
#
`sed s,/,\\,g < $OutDir/$SampleID/$SampleID.23andMePreOutput3.txt > $OutDir/$SampleID/$SampleID.23andMePreReport1.txt`;
#
`rm -f $OutDir/$SampleID/$SampleID.23andMePreOutput3.txt`;
#
#
######################################################################################################################





################# Change CHR ID and Column Order #############################################
#
open (IN, "$OutDir/$SampleID/$SampleID.23andMePreReport1.txt") or die;
open my $OFILE, '>', "$OutDir/$SampleID/$SampleID".'.23andMePreReport2vcf.txt' or die "Cannot create file for output: $!";

# go through the file line by line    
while (my $line = <IN>)  {

    # split the current line on tabs
    my @columns = split(/\t/, $line);
	
	chomp $columns[0];
	chomp $columns[1];
	chomp $columns[2];
	chomp $columns[3];	



	my $Chr_ID = $columns[1];
	$Chr_ID =~ s/chr//g;
	$Chr_ID =~ s/M/MT/g;

	
	my $GT_length = length($columns[3]);
	
	if ($GT_length == 2)
	{
		print $OFILE "$columns[0]\t$Chr_ID\t$columns[2]\t$columns[3]\n";
	}



	
}
# close the filehandle and exit
close $OFILE;
close IN;

#
`rm -f $OutDir/$SampleID/$SampleID.23andMePreReport1.txt`;
`rm -f $OutDir/$SampleID/$SampleID.VCFto23andMe.vcf`;
`rm -f $OutDir/$SampleID/$SampleID.VCFto23andMe.vcf.idx`;
`rm -f $OutDir/$SampleID/$SampleID.VCFto23andMe_GenoType.txt`;
#
######################################################################################################################




###### Determine Sex and Remove ChrY SNPs in Female Sample ##################################################
#
#This script utilizes the Heterozygosity ratio of ChrX and ChrY to determine the Sample Sex
#
#If the Sample is females, it skips the ChrY specific SNP's entries in the female female sample
#If Male, all the 23andMe SNP positions are reported
#
#
my $chrX_Het_Count = 0;
my $chrY_Het_Count = 0;
open (INCB, "$VCF_File") or die;
#
# go through the file line by line    
while (my $line = <INCB>)
{
	chomp ($line);
	chop($line) if ($line =~ m/\r$/);
    # split the current line on tabs
    my @columns = split(/\t/, $line);
	chomp $columns[0];
	chomp $columns[9];

	if ($columns[0] =~ m/chrX/ && $columns[9] !~ m/(^0\/0|^1\/1|^2\/2|^3\/3|^4\/4|^5\/5|^6\/6)/)
		{
		$chrX_Het_Count+=1;
		}

	if ($columns[0] =~ m/chrY/ && $columns[9] !~ m/(^0\/0|^1\/1|^2\/2|^3\/3|^4\/4|^5\/5|^6\/6)/)
		{
		$chrY_Het_Count+=1;
		}
		
	
}
print "\nHeterozygous ChrX Count: $chrX_Het_Count\n";
print "\nHeterozygous ChrY Count: $chrY_Het_Count\n";
my $Heterozygosity_ratio = $chrX_Het_Count/$chrY_Het_Count;
print "\nHeterozygosity ratio: $Heterozygosity_ratio\n";
#
# close the filehandle and exit
#
close INCB;
#
#
# Determine Sex
#
if ($Heterozygosity_ratio >= 1)
{
#
####Female Sample Processing
#
`cat $OutDir/$SampleID/$SampleID.23andMePreReport2vcf.txt $input23andMe_XX_vcf > $OutDir/$SampleID/$SampleID.23andMePreReport.Merge.txt`;
#
#Find Uniq Lines:
#
`sort $OutDir/$SampleID/$SampleID.23andMePreReport.Merge.txt | uniq -w10 -u > $OutDir/$SampleID/$SampleID.23andMePreReport.Uniq.txt`;
`rm -f $OutDir/$SampleID/$SampleID.23andMePreReport.Merge.txt`;
#
#Again Merge Uniq and Report File
#
`cat $OutDir/$SampleID/$SampleID.23andMePreReport2vcf.txt $OutDir/$SampleID/$SampleID.23andMePreReport.Uniq.txt > $OutDir/$SampleID/$SampleID.23andMePreReport3.txt`;
`rm -f $OutDir/$SampleID/$SampleID.23andMePreReport2vcf.txt`;
`rm -f $OutDir/$SampleID/$SampleID.23andMePreReport.Uniq.txt`;
#
#Again Sort the report FIle on ChrID and Position
#
`grep -v 'X' $OutDir/$SampleID/$SampleID.23andMePreReport3.txt | grep -v 'Y' | grep -v 'MT' | sort -n -k2 -k3 > $OutDir/$SampleID/$SampleID.VCFto23andMeReport.txt`;
`grep 'X' $OutDir/$SampleID/$SampleID.23andMePreReport3.txt | sort -n -k2 -k3 >> $OutDir/$SampleID/$SampleID.VCFto23andMeReport.txt`;
`grep 'Y' $OutDir/$SampleID/$SampleID.23andMePreReport3.txt | sort -n -k2 -k3 >> $OutDir/$SampleID/$SampleID.VCFto23andMeReport.txt`;
`grep 'MT' $OutDir/$SampleID/$SampleID.23andMePreReport3.txt | sort -n -k2 -k3 >> $OutDir/$SampleID/$SampleID.VCFto23andMeReport.txt`;
#
# Remove ChrY entries #
#
`sed -i.bak '/Y/d' $OutDir/$SampleID/$SampleID.VCFto23andMeReport.txt`;
#
#
`rm -f $OutDir/$SampleID/$SampleID.23andMePreReport3.txt`;
`rm -f $OutDir/$SampleID/$SampleID.VCFto23andMeReport.txt.bak`;
#
#
print "\nFinal Report Made at: ";
print `date`;
print "\n";
#
######################################################
}
else
{
#
##### Male Sample Processing
#
`cat $OutDir/$SampleID/$SampleID.23andMePreReport2vcf.txt $input23andMe_XY_vcf > $OutDir/$SampleID/$SampleID.23andMePreReport.Merge.txt`;
#
#Find Uniq Lines:
#
`sort $OutDir/$SampleID/$SampleID.23andMePreReport.Merge.txt | uniq -w10 -u > $OutDir/$SampleID/$SampleID.23andMePreReport.Uniq.txt`;
`rm -f $OutDir/$SampleID/$SampleID.23andMePreReport.Merge.txt`;
#
#Again Merge Uniq and Report File
#
`cat $OutDir/$SampleID/$SampleID.23andMePreReport2vcf.txt $OutDir/$SampleID/$SampleID.23andMePreReport.Uniq.txt > $OutDir/$SampleID/$SampleID.23andMePreReport3.txt`;
`rm -f $OutDir/$SampleID/$SampleID.23andMePreReport2vcf.txt`;
`rm -f $OutDir/$SampleID/$SampleID.23andMePreReport.Uniq.txt`;
#
#Again Sort the report FIle on ChrID and Position
#
`grep -v 'X' $OutDir/$SampleID/$SampleID.23andMePreReport3.txt | grep -v 'Y' | grep -v 'MT' | sort -n -k2 -k3 > $OutDir/$SampleID/$SampleID.VCFto23andMeReport.txt`;
`grep 'X' $OutDir/$SampleID/$SampleID.23andMePreReport3.txt | sort -n -k2 -k3 >> $OutDir/$SampleID/$SampleID.VCFto23andMeReport.txt`;
`grep 'Y' $OutDir/$SampleID/$SampleID.23andMePreReport3.txt | sort -n -k2 -k3 >> $OutDir/$SampleID/$SampleID.VCFto23andMeReport.txt`;
`grep 'MT' $OutDir/$SampleID/$SampleID.23andMePreReport3.txt | sort -n -k2 -k3 >> $OutDir/$SampleID/$SampleID.VCFto23andMeReport.txt`;
#
`rm -f $OutDir/$SampleID/$SampleID.23andMePreReport3.txt`;
#
}
#############################################################################################################

##### Add 3 Missing rsid (Duplicate positons but different ID name)
#
#
my $rs121912762 = `awk -v OFS='\t' '\$1 == "i5001703" {print \$2, \$3, \$4}' $OutDir/$SampleID/$SampleID.VCFto23andMeReport.txt`;
$rs121912762="rs121912762\t$rs121912762";
`perl -e 'print "$rs121912762"' >> $OutDir/$SampleID/$SampleID.VCFto23andMeReport.txt`;
#
my $rs121912763 = `awk -v OFS='\t' '\$1 == "i5001702" {print \$2, \$3, \$4}' $OutDir/$SampleID/$SampleID.VCFto23andMeReport.txt`;
$rs121912763="rs121912763\t$rs121912763";
`perl -e 'print "$rs121912763"' >> $OutDir/$SampleID/$SampleID.VCFto23andMeReport.txt`;
#
my $rs28933380 = `awk -v OFS='\t' '\$1 == "rs1801155" {print \$2, \$3, \$4}' $OutDir/$SampleID/$SampleID.VCFto23andMeReport.txt`;
$rs28933380="rs28933380\t$rs28933380";
`perl -e 'print "$rs28933380"' >> $OutDir/$SampleID/$SampleID.VCFto23andMeReport.txt`;
#
#
#Again Sort the report FIle on ChrID and Position
#
`grep -v 'X' $OutDir/$SampleID/$SampleID.VCFto23andMeReport.txt | grep -v 'Y' | grep -v 'MT' | sort -n -k2 -k3 > $OutDir/$SampleID/$SampleID.VCFto23nMeReport.txt`;
`grep 'X' $OutDir/$SampleID/$SampleID.VCFto23andMeReport.txt | sort -n -k2 -k3 >> $OutDir/$SampleID/$SampleID.VCFto23nMeReport.txt`;
`grep 'Y' $OutDir/$SampleID/$SampleID.VCFto23andMeReport.txt | sort -n -k2 -k3 >> $OutDir/$SampleID/$SampleID.VCFto23nMeReport.txt`;
`grep 'MT' $OutDir/$SampleID/$SampleID.VCFto23andMeReport.txt | sort -n -k2 -k3 >> $OutDir/$SampleID/$SampleID.VCFto23nMeReport.txt`;
#
`rm -f $OutDir/$SampleID/$SampleID.VCFto23andMeReport.txt`;
#
print "\nFinal Report Made at: ";
print `date`;
print "\n";
#
##############################################################################################################