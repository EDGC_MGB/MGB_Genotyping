#!/usr/bin/env/perl
#use strict;
#use warnings;

## Processing INPUT Command Line arguments
#
#
my $VCF= $ARGV[0];
my $SampleID= $ARGV[1];
my $OutDir= $ARGV[2];
#
chomp $VCF;
chomp $SampleID;
chomp $OutDir;


	###### Determine Sex and Remove ChrY SNPs in Female Sample ########################################################
	#
	#This script utilizes the Heterozygosity ratio of ChrX and ChrY to determine the Sample Sex
	#
	#If the Sample is females, it skips the ChrY specific SNP's entries in the Final Report
	#If Male, all the dbsnp loci find in the Sample are reported
	#
	#
	my $chrX_Het_Count = 0;
	my $chrY_Het_Count = 0;
	open (INCB, "$VCF") or die;
	#
	# go through the file line by line    
	while (my $line = <INCB>)
	{
		chomp ($line);
		chop($line) if ($line =~ m/\r$/);
		# split the current line on tabs
		my @columns = split(/\t/, $line);
		chomp $columns[0];
		chomp $columns[9];

		if ($columns[0] =~ m/chrX/ && $columns[9] !~ m/(^0\/0|^1\/1|^2\/2|^3\/3|^4\/4|^5\/5|^6\/6)/)
			{
			$chrX_Het_Count+=1;
			}

		if ($columns[0] =~ m/chrY/ && $columns[9] !~ m/(^0\/0|^1\/1|^2\/2|^3\/3|^4\/4|^5\/5|^6\/6)/)
			{
			$chrY_Het_Count+=1;
			}
	}

	my $Heterozygosity_ratio = $chrX_Het_Count/$chrY_Het_Count;
	print "\nHeterozygosity ratio: $Heterozygosity_ratio";
	#
	# close the filehandle and exit
	#
	close INCB;
	#
	#
	# Determine Sex
	#
	if ($Heterozygosity_ratio >= 1)
	{
	#
	print "\nSample's Gender is: Female";
	#
	####Female Sample Processing
	#
	# Sort the report FIle on ChrID and Position
	#
	`grep -v 'X' $OutDir/$SampleID/$SampleID.dbsnp147_plus_Informative_GT.txt | grep -v 'Y' | grep -v 'MT' > $OutDir/$SampleID/$SampleID.VCF_and_gVCF_to_dbsnp147_plus_SNP_INDEL_GT_Report.txt`;
	`grep 'X' $OutDir/$SampleID/$SampleID.dbsnp147_plus_Informative_GT.txt >> $OutDir/$SampleID/$SampleID.VCF_and_gVCF_to_dbsnp147_plus_SNP_INDEL_GT_Report.txt`;
	`grep 'MT' $OutDir/$SampleID/$SampleID.dbsnp147_plus_Informative_GT.txt >> $OutDir/$SampleID/$SampleID.VCF_and_gVCF_to_dbsnp147_plus_SNP_INDEL_GT_Report.txt`;
	#
	#
	system("awk '{ if ((length(\$4) <= 3 ) ) print; }' $SampleID.VCF_and_gVCF_to_dbsnp147_plus_SNP_INDEL_GT_Report.txt > $SampleID.VCF_and_gVCF_to_dbsnp147_plus_SNP_GT_PreReport.txt");
	system("sed 's|/||g' $SampleID.VCF_and_gVCF_to_dbsnp147_plus_SNP_GT_PreReport.txt > $SampleID.VCF_and_gVCF_to_dbsnp147_plus_SNP_GT_Report.txt");
	##system("awk 'FNR==NR {a[\$1]; next}; \$1 in a' $MyPath/MGB_450Marker.txt $OutDir/$SampleID/$SampleID.VCF_and_gVCF_to_dbsnp147_plus_SNP_GT_Report.txt > $SampleID.VCF_and_gVCF_to_dbsnp147_plus.MGB_450Marker.txt");
	`gzip $OutDir/$SampleID/$SampleID.VCF_and_gVCF_to_dbsnp147_plus_SNP_GT_Report.txt`;
	
	`rm -f $OutDir/$SampleID/$SampleID.dbsnp147_plus_Informative_GT.txt`;
	`rm -f $OutDir/$SampleID/$SampleID.VCF_and_gVCF_to_dbsnp147_plus_SNP_INDEL_GT_Report.txt`;
	`rm -f $OutDir/$SampleID/$SampleID.VCF_and_gVCF_to_dbsnp147_plus_SNP_GT_PreReport.txt`;
	print "\nFinal Report Made at: ";
	print `date`;
	print "\n";
	#
	#############################################
	}
	else
	{
	#
	print "\nSample's Gender is: Male";
	#
	##### Male Sample Processing
	#
	# Sort the report FIle on ChrID and Position
	#
	`grep -v 'X' $OutDir/$SampleID/$SampleID.dbsnp147_plus_Informative_GT.txt | grep -v 'Y' | grep -v 'MT' > $OutDir/$SampleID/$SampleID.VCF_and_gVCF_to_dbsnp147_plus_SNP_INDEL_GT_Report.txt`;
	`grep 'X' $OutDir/$SampleID/$SampleID.dbsnp147_plus_Informative_GT.txt >> $OutDir/$SampleID/$SampleID.VCF_and_gVCF_to_dbsnp147_plus_SNP_INDEL_GT_Report.txt`;
	`grep 'Y' $OutDir/$SampleID/$SampleID.dbsnp147_plus_Informative_GT.txt >> $OutDir/$SampleID/$SampleID.VCF_and_gVCF_to_dbsnp147_plus_SNP_INDEL_GT_Report.txt`;
	`grep 'MT' $OutDir/$SampleID/$SampleID.dbsnp147_plus_Informative_GT.txt >> $OutDir/$SampleID/$SampleID.VCF_and_gVCF_to_dbsnp147_plus_SNP_INDEL_GT_Report.txt`;
	#
	#
	system("awk '{ if ((length(\$4) <= 3 ) ) print; }' $SampleID.VCF_and_gVCF_to_dbsnp147_plus_SNP_INDEL_GT_Report.txt > $SampleID.VCF_and_gVCF_to_dbsnp147_plus_SNP_GT_PreReport.txt");
	system("sed 's|/||g' $SampleID.VCF_and_gVCF_to_dbsnp147_plus_SNP_GT_PreReport.txt > $SampleID.VCF_and_gVCF_to_dbsnp147_plus_SNP_GT_Report.txt");
	##system("awk 'FNR==NR {a[\$1]; next}; \$1 in a' $MyPath/MGB_450Marker.txt $OutDir/$SampleID/$SampleID.VCF_and_gVCF_to_dbsnp147_plus_SNP_GT_Report.txt > $SampleID.VCF_and_gVCF_to_dbsnp147_plus.MGB_450Marker.txt");
	`gzip $OutDir/$SampleID/$SampleID.VCF_and_gVCF_to_dbsnp147_plus_SNP_GT_Report.txt`;
	
	`rm -f $OutDir/$SampleID/$SampleID.dbsnp147_plus_Informative_GT.txt`;
	`rm -f $OutDir/$SampleID/$SampleID.VCF_and_gVCF_to_dbsnp147_plus_SNP_INDEL_GT_Report.txt`;
	`rm -f $OutDir/$SampleID/$SampleID.VCF_and_gVCF_to_dbsnp147_plus_SNP_GT_PreReport.txt`;
	print "\nFinal Report Made at: ";
	print `date`;
	print "\n";
	}
	#
	###################################################################################################################