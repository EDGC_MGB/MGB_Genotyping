#!/usr/bin/env/perl
#use strict;
#use warnings;
#
#############################################################################################################
##
#This script is developed at EONE-DIAGNOMICS Genome Center (www.edgc.com) to extract the Genotype information from the gvcf file.
#
#This (Variants-to-dbsnp147-GT_v.pl) script requires Dragen/GATK VCF, gVCF file, sampleID/sampleName and OutDir Location as input
#
#############################################################################################################
#############################################################################################################
#Usage Example:#
# e.g., perl /Path_to_script/Variants-to-dbsnp147-GT_v3.pl ././MGB_xxxxxx.vcf MGB_xxxxxx /staging/samples/MGB_WGS/MGB_xxxxxx
# e.g., perl /Path_to_script/Variants-to-dbsnp147-GT_v3.pl ././MGB_xxxxxx.gvcf MGB_xxxxxx /staging/samples/MGB_WGS/MGB_xxxxxx
# e.g., perl /Path_to_script/Variants-to-dbsnp147-GT_v3.pl ././MGB_xxxxxx.vcf ././MGB_xxxxxx.gvcf MGB_xxxxxx /staging/samples/MGB_WGS/MGB_xxxxxx
#
#############################################################################################################
#############################################################################################################
#Please provide full path of the perl script as well as VCF and gvcf files.
#Contact Dr. Amit Goyal at a.goyal@edgc.com for any query.
#
#############################################################################################################
#############################################################################################################
#
#
use File::Basename;
my $path = "$0";
my($filename, $MyPath, $suffix) = fileparse($path);
$MyPath =~ s/\/$//;
#
#
#############################################################################################################
#
## Variants-to-dbsnp147-GT.pl script needs the below files during the processing ####
## Also, Keep the tabix indexed file "dbsnp147.vcf.gz.tbi" at the same location with the "dbsnp147.vcf.gz" file
#
my $Extract_GT_gVCF_sh = "$MyPath/Extract_gVCF_Only_dbsnp147_Genotype_v3.sh";			#### Check the location
my $Extract_GT_VCF_sh = "$MyPath/Extract_VCF_Only_dbsnp147_Genotype_v3.sh";				#### Check the location
my $Extract_GT_VCF_gVCF_sh = "$MyPath/Extract_VCF_gVCF_dbsnp147_Genotype_v3.sh";		#### Check the location
$MyPath =~ s/src/data/i;
my $bed_dbsnp147 = "$MyPath/v3/dbsnp147_plus.bed";										#### Check the location								
my $SNPid_dbsnp147_vcf = "$MyPath/v3/dbsnp147_plus.vcf.gz";								#### Check the location

#
##############################################################################################################
#
## Processing INPUT Command Line arguments
#
#
my $arg1= $ARGV[0];
my $arg2= $ARGV[1];
my $arg3= $ARGV[2];
my $arg4= $ARGV[3];
#
chomp $arg1;
chomp $arg2;
chomp $arg3;
chomp $arg4;
#
#############################################################################################################
#############################################################################################################


#############################################################################################################
#############################################################################################################
# (1) quit unless we have the correct number of command-line args
$num_args = $#ARGV + 1;
if ($num_args < 3 || $arg1 eq "-h")
{
	print "\nERROR: Required Minimum 3 Arguments \n\n";
	print "\nVariant File to Genotyping Script: This script is developed to convert the variants in the VCF/gVCF (or both) Files to the 23andMe like Genotyping Table.";
    print "\nUsage Examples:";
	print "\nperl /Path_to_script/Variants-to-dbsnp147-GT_v3.pl SampleID.vcf SampleID.gvcf SampleID path_to_OutputDir";
	print "\nOR";
	print "\nperl /Path_to_script/Variants-to-dbsnp147-GT_v3.pl SampleID.gvcf SampleID path_to_OutputDir";
	print "\nOR";
	print "\nperl /Path_to_script/Variants-to-dbsnp147-GT_v3.pl SampleID.vcf SampleID path_to_OutputDir\n\n";
    exit;
}
#############################################################################################################
#############################################################################################################


#############################################################################################################
#############################################################################################################
if ($num_args < 4)
{
	my ($arg1_ext) = $arg1 =~ /(\.[^.]+)$/;
	if ($arg1_ext eq ".gvcf")
	{
	my $gVCF_File = $arg1;
	my $SampleID = $arg2;
	my $OutDir = $arg3;
	

	print "\nStarting time: ";
	print `date`;

	`mkdir $OutDir/$SampleID`;
	chdir "/$OutDir/$SampleID";
	
	################# Extract Genotype from using the gVCF and VCF File ###############################################
	#
	#
	`sh $Extract_GT_gVCF_sh $gVCF_File $SampleID $MyPath`;
	#
	#
	#It writes the all Loci GT results to the "$SampleID.dbsnp147_Informative_GT.txt" to be processed below. 
	#
	###################################################################################################################


	###### Determine Sex and Remove ChrY SNPs in Female Sample ########################################################
	#
	#This script utilizes the Heterozygosity ratio of ChrX and ChrY to determine the Sample Sex
	#
	#If the Sample is females, it skips the ChrY specific SNP's entries in the Final Report
	#If Male, all the dbsnp loci find in the Sample are reported
	#
	#
	my $chrX_Het_Count = 0;
	my $chrY_Het_Count = 0;
	open (INCB, "$OutDir/$SampleID/$SampleID.Variant.vcf") or die;
	#
	# go through the file line by line    
	while (my $line = <INCB>)
	{
		chomp ($line);
		chop($line) if ($line =~ m/\r$/);
		# split the current line on tabs
		my @columns = split(/\t/, $line);
		chomp $columns[0];
		chomp $columns[9];

		if ($columns[0] =~ m/chrX/ && $columns[9] !~ m/(^0\/0|^1\/1|^2\/2|^3\/3|^4\/4|^5\/5|^6\/6)/)
			{
			$chrX_Het_Count+=1;
			}

		if ($columns[0] =~ m/chrY/ && $columns[9] !~ m/(^0\/0|^1\/1|^2\/2|^3\/3|^4\/4|^5\/5|^6\/6)/)
			{
			$chrY_Het_Count+=1;
			}
	}

	my $Heterozygosity_ratio = $chrX_Het_Count/$chrY_Het_Count;
	print "\nHeterozygosity ratio: $Heterozygosity_ratio";
	#
	# close the filehandle and exit
	#
	close INCB;
	#
	#
	# Determine Sex
	#
	if ($Heterozygosity_ratio >= 4)
	{
	#
	print "\nSample's Gender is: Female";
	#
	####Female Sample Processing
	#
	# Sort the report FIle on ChrID and Position
	#
	`grep -v 'X' $OutDir/$SampleID/$SampleID.dbsnp147_plus_Informative_GT.txt | grep -v 'Y' | grep -v 'MT' > $OutDir/$SampleID/$SampleID.gVCF_to_dbsnp147_plus_SNP_INDEL_GT_Report.txt`;
	`grep 'X' $OutDir/$SampleID/$SampleID.dbsnp147_plus_Informative_GT.txt >> $OutDir/$SampleID/$SampleID.gVCF_to_dbsnp147_plus_SNP_INDEL_GT_Report.txt`;
	`grep 'MT' $OutDir/$SampleID/$SampleID.dbsnp147_plus_Informative_GT.txt >> $OutDir/$SampleID/$SampleID.gVCF_to_dbsnp147_plus_SNP_INDEL_GT_Report.txt`;
	#
	#
	system("awk '{ if ((length(\$4) <= 3 ) ) print; }' $SampleID.gVCF_to_dbsnp147_plus_SNP_INDEL_GT_Report.txt > $SampleID.gVCF_to_dbsnp147_plus_SNP_GT_PreReport.txt");
	system("sed 's|/||g' $SampleID.gVCF_to_dbsnp147_plus_SNP_GT_PreReport.txt > $SampleID.gVCF_to_dbsnp147_plus_SNP_GT_Report.txt");
	system("awk 'FNR==NR {a[\$1]; next}; \$1 in a' $MyPath/MGB_450Marker.txt $OutDir/$SampleID/$SampleID.gVCF_to_dbsnp147_plus_SNP_GT_Report.txt > $SampleID.gVCF_to_dbsnp147_plus.MGB_450Marker.txt");
	`gzip $OutDir/$SampleID/$SampleID.gVCF_to_dbsnp147_plus_SNP_GT_Report.txt`;
	
	`rm -f $OutDir/$SampleID/$SampleID.dbsnp147_plus_Informative_GT.txt`;
	`rm -f $OutDir/$SampleID/$SampleID.gVCF_to_dbsnp147_plus_SNP_INDEL_GT_Report.txt`;
	`rm -f $OutDir/$SampleID/$SampleID.gVCF_to_dbsnp147_plus_SNP_GT_PreReport.txt`;
	print "\nFinal Report Made at: ";
	print `date`;
	print "\n";
	#
	#############################################
	}
	else
	{
	#
	print "\nSample's Gender is: Male";
	#
	##### Male Sample Processing
	#
	# Sort the report FIle on ChrID and Position
	#
	`grep -v 'X' $OutDir/$SampleID/$SampleID.dbsnp147_plus_Informative_GT.txt | grep -v 'Y' | grep -v 'MT' > $OutDir/$SampleID/$SampleID.gVCF_to_dbsnp147_plus_SNP_INDEL_GT_Report.txt`;
	`grep 'X' $OutDir/$SampleID/$SampleID.dbsnp147_plus_Informative_GT.txt >> $OutDir/$SampleID/$SampleID.gVCF_to_dbsnp147_plus_SNP_INDEL_GT_Report.txt`;
	`grep 'Y' $OutDir/$SampleID/$SampleID.dbsnp147_plus_Informative_GT.txt >> $OutDir/$SampleID/$SampleID.gVCF_to_dbsnp147_plus_SNP_INDEL_GT_Report.txt`;
	`grep 'MT' $OutDir/$SampleID/$SampleID.dbsnp147_plus_Informative_GT.txt >> $OutDir/$SampleID/$SampleID.gVCF_to_dbsnp147_plus_SNP_INDEL_GT_Report.txt`;
	#
	#
	system("awk '{ if ((length(\$4) <= 3 ) ) print; }' $SampleID.gVCF_to_dbsnp147_plus_SNP_INDEL_GT_Report.txt > $SampleID.gVCF_to_dbsnp147_plus_SNP_GT_PreReport.txt");
	system("sed 's|/||g' $SampleID.gVCF_to_dbsnp147_plus_SNP_GT_PreReport.txt > $SampleID.gVCF_to_dbsnp147_plus_SNP_GT_Report.txt");
	system("awk 'FNR==NR {a[\$1]; next}; \$1 in a' $MyPath/MGB_450Marker.txt $OutDir/$SampleID/$SampleID.gVCF_to_dbsnp147_plus_SNP_GT_Report.txt > $SampleID.gVCF_to_dbsnp147_plus.MGB_450Marker.txt");
	`gzip $OutDir/$SampleID/$SampleID.gVCF_to_dbsnp147_plus_SNP_GT_Report.txt`;
	
	`rm -f $OutDir/$SampleID/$SampleID.dbsnp147_plus_Informative_GT.txt`;
	`rm -f $OutDir/$SampleID/$SampleID.gVCF_to_dbsnp147_plus_SNP_INDEL_GT_Report.txt`;
	`rm -f $OutDir/$SampleID/$SampleID.gVCF_to_dbsnp147_plus_SNP_GT_PreReport.txt`;
	print "\nFinal Report Made at: ";
	print `date`;
	print "\n";
	}
	#
	`rm -f $OutDir/$SampleID/$SampleID.Variant*`;
	###################################################################################################################

	
	}
		elsif($arg1_ext eq ".vcf")
			{
			my $VCF_File = $arg1;
			my $SampleID = $arg2;
			my $OutDir = $arg3;
			

			print "\nStarting time: ";
			print `date`;

			`mkdir $OutDir/$SampleID`;
			chdir "/$OutDir/$SampleID";
			
			################# Extract Genotype from using the gVCF and VCF File ###############################################
			#
			#
			`sh $Extract_GT_VCF_sh $VCF_File $SampleID $MyPath`;
			#
			#
			#It writes the all Loci GT results to the "$SampleID.dbsnp147_Informative_GT.txt" to be processed below. 
			#
			###################################################################################################################


			###### Determine Sex and Remove ChrY SNPs in Female Sample ########################################################
			#
			#This script utilizes the Heterozygosity ratio of ChrX and ChrY to determine the Sample Sex
			#
			#If the Sample is females, it skips the ChrY specific SNP's entries in the Final Report
			#If Male, all the dbsnp loci find in the Sample are reported
			#
			#
			my $chrX_Het_Count = 0;
			my $chrY_Het_Count = 0;
			open (INCB, "$VCF_File") or die;
			#
			# go through the file line by line    
			while (my $line = <INCB>)
			{
				chomp ($line);
				chop($line) if ($line =~ m/\r$/);
				# split the current line on tabs
				my @columns = split(/\t/, $line);
				chomp $columns[0];
				chomp $columns[9];

				if ($columns[0] =~ m/chrX/ && $columns[9] !~ m/(^0\/0|^1\/1|^2\/2|^3\/3|^4\/4|^5\/5|^6\/6)/)
					{
					$chrX_Het_Count+=1;
					}

				if ($columns[0] =~ m/chrY/ && $columns[9] !~ m/(^0\/0|^1\/1|^2\/2|^3\/3|^4\/4|^5\/5|^6\/6)/)
					{
					$chrY_Het_Count+=1;
					}
			}

			my $Heterozygosity_ratio = $chrX_Het_Count/$chrY_Het_Count;
			print "\nHeterozygosity ratio: $Heterozygosity_ratio";
			#
			# close the filehandle and exit
			#
			close INCB;
			#
			#
			# Determine Sex
			#
			if ($Heterozygosity_ratio >= 1)
			{
			#
			print "\nSample's Gender is: Female";
			#
			####Female Sample Processing
			#
			# Sort the report FIle on ChrID and Position
			#
			`grep -v 'X' $OutDir/$SampleID/$SampleID.dbsnp147_plus_Informative_GT.txt | grep -v 'Y' | grep -v 'MT' > $OutDir/$SampleID/$SampleID.VCF_to_dbsnp147_plus_SNP_INDEL_GT_Report.txt`;
			`grep 'X' $OutDir/$SampleID/$SampleID.dbsnp147_plus_Informative_GT.txt >> $OutDir/$SampleID/$SampleID.VCF_to_dbsnp147_plus_SNP_INDEL_GT_Report.txt`;
			`grep 'MT' $OutDir/$SampleID/$SampleID.dbsnp147_plus_Informative_GT.txt >> $OutDir/$SampleID/$SampleID.VCF_to_dbsnp147_plus_SNP_INDEL_GT_Report.txt`;
			#
			#
			system("awk '{ if ((length(\$4) <= 3 ) ) print; }' $SampleID.VCF_to_dbsnp147_plus_SNP_INDEL_GT_Report.txt > $SampleID.VCF_to_dbsnp147_plus_SNP_GT_PreReport.txt");
			system("sed 's|/||g' $SampleID.VCF_to_dbsnp147_plus_SNP_GT_PreReport.txt > $SampleID.VCF_to_dbsnp147_plus_SNP_GT_Report.txt");
			system("awk 'FNR==NR {a[\$1]; next}; \$1 in a' $MyPath/MGB_450Marker.txt $OutDir/$SampleID/$SampleID.VCF_to_dbsnp147_plus_SNP_GT_Report.txt > $SampleID.VCF_to_dbsnp147_plus.MGB_450Marker.txt");
			`gzip $OutDir/$SampleID/$SampleID.VCF_to_dbsnp147_plus_SNP_GT_Report.txt`;
			
			`rm -f $OutDir/$SampleID/$SampleID.dbsnp147_plus_Informative_GT.txt`;
			`rm -f $OutDir/$SampleID/$SampleID.VCF_to_dbsnp147_plus_SNP_INDEL_GT_Report.txt`;
			`rm -f $OutDir/$SampleID/$SampleID.VCF_to_dbsnp147_plus_SNP_GT_PreReport.txt`;
			print "\nFinal Report Made at: ";
			print `date`;
			print "\n";
			#
			#############################################
			}
			else
			{
			#
			print "\nSample's Gender is: Male";
			#
			##### Male Sample Processing
			#
			# Sort the report FIle on ChrID and Position
			#
			`grep -v 'X' $OutDir/$SampleID/$SampleID.dbsnp147_plus_Informative_GT.txt | grep -v 'Y' | grep -v 'MT' > $OutDir/$SampleID/$SampleID.VCF_to_dbsnp147_plus_SNP_INDEL_GT_Report.txt`;
			`grep 'X' $OutDir/$SampleID/$SampleID.dbsnp147_plus_Informative_GT.txt >> $OutDir/$SampleID/$SampleID.VCF_to_dbsnp147_plus_SNP_INDEL_GT_Report.txt`;
			`grep 'Y' $OutDir/$SampleID/$SampleID.dbsnp147_plus_Informative_GT.txt >> $OutDir/$SampleID/$SampleID.VCF_to_dbsnp147_plus_SNP_INDEL_GT_Report.txt`;
			`grep 'MT' $OutDir/$SampleID/$SampleID.dbsnp147_plus_Informative_GT.txt >> $OutDir/$SampleID/$SampleID.VCF_to_dbsnp147_plus_SNP_INDEL_GT_Report.txt`;
			#
			#
			system("awk '{ if ((length(\$4) <= 3 ) ) print; }' $SampleID.VCF_to_dbsnp147_plus_SNP_INDEL_GT_Report.txt > $SampleID.VCF_to_dbsnp147_plus_SNP_GT_PreReport.txt");
			system("sed 's|/||g' $SampleID.VCF_to_dbsnp147_plus_SNP_GT_PreReport.txt > $SampleID.VCF_to_dbsnp147_plus_SNP_GT_Report.txt");
			system("awk 'FNR==NR {a[\$1]; next}; \$1 in a' $MyPath/MGB_450Marker.txt $OutDir/$SampleID/$SampleID.VCF_to_dbsnp147_plus_SNP_GT_Report.txt > $SampleID.VCF_to_dbsnp147_plus.MGB_450Marker.txt");
			`gzip $OutDir/$SampleID/$SampleID.VCF_to_dbsnp147_plus_SNP_GT_Report.txt`;
			
			`rm -f $OutDir/$SampleID/$SampleID.dbsnp147_plus_Informative_GT.txt`;
			`rm -f $OutDir/$SampleID/$SampleID.VCF_to_dbsnp147_plus_SNP_INDEL_GT_Report.txt`;
			`rm -f $OutDir/$SampleID/$SampleID.VCF_to_dbsnp147_plus_SNP_GT_PreReport.txt`;

			print "\nFinal Report Made at: ";
			print `date`;
			print "\n";
			}
			#
			`rm -f $OutDir/$SampleID/$SampleID.Variant*`;
			###################################################################################################################
			
			}		
			else
				{
				print "\nERROR: CHECK Input Arguments \n\n";
				print "\nVariant File to Genotyping Script: This script is developed to convert the variants in the VCF/gVCF (or both) Files to the 23andMe like Genotyping Table.";
				print "\nUsage Examples:";
				print "\nperl /Path_to_script/Variants-to-dbsnp147-GT_v3.pl SampleID.vcf SampleID.gvcf SampleID path_to_OutputDir";
				print "\nOR";
				print "\nperl /Path_to_script/Variants-to-dbsnp147-GT_v3.pl SampleID.gvcf SampleID path_to_OutputDir";
				print "\nOR";
				print "\nperl /Path_to_script/Variants-to-dbsnp147-GT_v3.pl SampleID.vcf SampleID path_to_OutputDir\n\n";
				exit;
				}
}
#############################################################################################################
#############################################################################################################


#############################################################################################################
#############################################################################################################
if ($num_args == 4)
{
	my ($arg1_ext) = $arg1 =~ /(\.[^.]+)$/;
	my ($arg2_ext) = $arg2 =~ /(\.[^.]+)$/;
	
	if ($arg1_ext =~ /.vcf/ && $arg2_ext =~ /.gvcf/)
	{
	my $VCF_File = $arg1;
	my $gVCF_File = $arg2;
	my $SampleID = $arg3;
	my $OutDir = $arg4;
	

	print "\nStarting time: ";
	print `date`;

	`mkdir $OutDir/$SampleID`;
	chdir "/$OutDir/$SampleID";
	
	################# Extract Genotype from using the gVCF and VCF File ###############################################
	#
	#
	`sh $Extract_GT_VCF_gVCF_sh $VCF_File $gVCF_File $SampleID $MyPath`;
	#
	#
	#It writes the all Loci GT results to the "$SampleID.dbsnp147_Informative_GT.txt" to be processed below. 
	#
	###################################################################################################################


	###### Determine Sex and Remove ChrY SNPs in Female Sample ########################################################
	#
	#This script utilizes the Heterozygosity ratio of ChrX and ChrY to determine the Sample Sex
	#
	#If the Sample is females, it skips the ChrY specific SNP's entries in the Final Report
	#If Male, all the dbsnp loci find in the Sample are reported
	#
	#
	my $chrX_Het_Count = 0;
	my $chrY_Het_Count = 0;
	open (INCB, "$VCF_File") or die;
	#
	# go through the file line by line    
	while (my $line = <INCB>)
	{
		chomp ($line);
		chop($line) if ($line =~ m/\r$/);
		# split the current line on tabs
		my @columns = split(/\t/, $line);
		chomp $columns[0];
		chomp $columns[9];

		if ($columns[0] =~ m/chrX/ && $columns[9] !~ m/(^0\/0|^1\/1|^2\/2|^3\/3|^4\/4|^5\/5|^6\/6)/)
			{
			$chrX_Het_Count+=1;
			}

		if ($columns[0] =~ m/chrY/ && $columns[9] !~ m/(^0\/0|^1\/1|^2\/2|^3\/3|^4\/4|^5\/5|^6\/6)/)
			{
			$chrY_Het_Count+=1;
			}
	}

	my $Heterozygosity_ratio = $chrX_Het_Count/$chrY_Het_Count;
	print "\nHeterozygosity ratio: $Heterozygosity_ratio";
	#
	# close the filehandle and exit
	#
	close INCB;
	#
	#
	# Determine Sex
	#
	if ($Heterozygosity_ratio >= 1)
	{
	#
	print "\nSample's Gender is: Female";
	#
	####Female Sample Processing
	#
	# Sort the report FIle on ChrID and Position
	#
	`grep -v 'X' $OutDir/$SampleID/$SampleID.dbsnp147_plus_Informative_GT.txt | grep -v 'Y' | grep -v 'MT' > $OutDir/$SampleID/$SampleID.VCF_and_gVCF_to_dbsnp147_plus_SNP_INDEL_GT_Report.txt`;
	`grep 'X' $OutDir/$SampleID/$SampleID.dbsnp147_plus_Informative_GT.txt >> $OutDir/$SampleID/$SampleID.VCF_and_gVCF_to_dbsnp147_plus_SNP_INDEL_GT_Report.txt`;
	`grep 'MT' $OutDir/$SampleID/$SampleID.dbsnp147_plus_Informative_GT.txt >> $OutDir/$SampleID/$SampleID.VCF_and_gVCF_to_dbsnp147_plus_SNP_INDEL_GT_Report.txt`;
	#
	#
	system("awk '{ if ((length(\$4) <= 3 ) ) print; }' $SampleID.VCF_and_gVCF_to_dbsnp147_plus_SNP_INDEL_GT_Report.txt > $SampleID.VCF_and_gVCF_to_dbsnp147_plus_SNP_GT_PreReport.txt");
	system("sed 's|/||g' $SampleID.VCF_and_gVCF_to_dbsnp147_plus_SNP_GT_PreReport.txt > $SampleID.VCF_and_gVCF_to_dbsnp147_plus_SNP_GT_Report.txt");
	system("awk 'FNR==NR {a[\$1]; next}; \$1 in a' $MyPath/MGB_450Marker.txt $OutDir/$SampleID/$SampleID.VCF_and_gVCF_to_dbsnp147_plus_SNP_GT_Report.txt > $SampleID.VCF_and_gVCF_to_dbsnp147_plus.MGB_450Marker.txt");
	`gzip $OutDir/$SampleID/$SampleID.VCF_and_gVCF_to_dbsnp147_plus_SNP_GT_Report.txt`;
	
	`rm -f $OutDir/$SampleID/$SampleID.dbsnp147_plus_Informative_GT.txt`;
	`rm -f $OutDir/$SampleID/$SampleID.VCF_and_gVCF_to_dbsnp147_plus_SNP_INDEL_GT_Report.txt`;
	`rm -f $OutDir/$SampleID/$SampleID.VCF_and_gVCF_to_dbsnp147_plus_SNP_GT_PreReport.txt`;
	print "\nFinal Report Made at: ";
	print `date`;
	print "\n";
	#
	#############################################
	}
	else
	{
	#
	print "\nSample's Gender is: Male";
	#
	##### Male Sample Processing
	#
	# Sort the report FIle on ChrID and Position
	#
	`grep -v 'X' $OutDir/$SampleID/$SampleID.dbsnp147_plus_Informative_GT.txt | grep -v 'Y' | grep -v 'MT' > $OutDir/$SampleID/$SampleID.VCF_and_gVCF_to_dbsnp147_plus_SNP_INDEL_GT_Report.txt`;
	`grep 'X' $OutDir/$SampleID/$SampleID.dbsnp147_plus_Informative_GT.txt >> $OutDir/$SampleID/$SampleID.VCF_and_gVCF_to_dbsnp147_plus_SNP_INDEL_GT_Report.txt`;
	`grep 'Y' $OutDir/$SampleID/$SampleID.dbsnp147_plus_Informative_GT.txt >> $OutDir/$SampleID/$SampleID.VCF_and_gVCF_to_dbsnp147_plus_SNP_INDEL_GT_Report.txt`;
	`grep 'MT' $OutDir/$SampleID/$SampleID.dbsnp147_plus_Informative_GT.txt >> $OutDir/$SampleID/$SampleID.VCF_and_gVCF_to_dbsnp147_plus_SNP_INDEL_GT_Report.txt`;
	#
	#
	system("awk '{ if ((length(\$4) <= 3 ) ) print; }' $SampleID.VCF_and_gVCF_to_dbsnp147_plus_SNP_INDEL_GT_Report.txt > $SampleID.VCF_and_gVCF_to_dbsnp147_plus_SNP_GT_PreReport.txt");
	system("sed 's|/||g' $SampleID.VCF_and_gVCF_to_dbsnp147_plus_SNP_GT_PreReport.txt > $SampleID.VCF_and_gVCF_to_dbsnp147_plus_SNP_GT_Report.txt");
	system("awk 'FNR==NR {a[\$1]; next}; \$1 in a' $MyPath/MGB_450Marker.txt $OutDir/$SampleID/$SampleID.VCF_and_gVCF_to_dbsnp147_plus_SNP_GT_Report.txt > $SampleID.VCF_and_gVCF_to_dbsnp147_plus.MGB_450Marker.txt");
	`gzip $OutDir/$SampleID/$SampleID.VCF_and_gVCF_to_dbsnp147_plus_SNP_GT_Report.txt`;
	
	`rm -f $OutDir/$SampleID/$SampleID.dbsnp147_plus_Informative_GT.txt`;
	`rm -f $OutDir/$SampleID/$SampleID.VCF_and_gVCF_to_dbsnp147_plus_SNP_INDEL_GT_Report.txt`;
	`rm -f $OutDir/$SampleID/$SampleID.VCF_and_gVCF_to_dbsnp147_plus_SNP_GT_PreReport.txt`;
	print "\nFinal Report Made at: ";
	print `date`;
	print "\n";
	}
	#
	###################################################################################################################
	}
	
			elsif ($arg1_ext =~ /.gvcf/ && $arg2_ext =~ /.vcf/)
			{
			my $VCF_File = $arg2;
			my $gVCF_File = $arg1;
			my $SampleID = $arg3;
			my $OutDir = $arg4;
			

			print "\nStarting time: ";
			print `date`;

			`mkdir $OutDir/$SampleID`;
			chdir "/$OutDir/$SampleID";
			
			################# Extract Genotype from using the gVCF and VCF File ###############################################
			#
			#
			`sh $Extract_GT_VCF_gVCF_sh $VCF_File $gVCF_File $SampleID $MyPath`;
			#
			#
			#It writes the all Loci GT results to the "$SampleID.dbsnp147_Informative_GT.txt" to be processed below. 
			#
			###################################################################################################################


			###### Determine Sex and Remove ChrY SNPs in Female Sample ########################################################
			#
			#This script utilizes the Heterozygosity ratio of ChrX and ChrY to determine the Sample Sex
			#
			#If the Sample is females, it skips the ChrY specific SNP's entries in the Final Report
			#If Male, all the dbsnp loci find in the Sample are reported
			#
			#
			my $chrX_Het_Count = 0;
			my $chrY_Het_Count = 0;
			open (INCB, "$VCF_File") or die;
			#
			# go through the file line by line    
			while (my $line = <INCB>)
			{
				chomp ($line);
				chop($line) if ($line =~ m/\r$/);
				# split the current line on tabs
				my @columns = split(/\t/, $line);
				chomp $columns[0];
				chomp $columns[9];

				if ($columns[0] =~ m/chrX/ && $columns[9] !~ m/(^0\/0|^1\/1|^2\/2|^3\/3|^4\/4|^5\/5|^6\/6)/)
					{
					$chrX_Het_Count+=1;
					}

				if ($columns[0] =~ m/chrY/ && $columns[9] !~ m/(^0\/0|^1\/1|^2\/2|^3\/3|^4\/4|^5\/5|^6\/6)/)
					{
					$chrY_Het_Count+=1;
					}
			}

			my $Heterozygosity_ratio = $chrX_Het_Count/$chrY_Het_Count;
			print "\nHeterozygosity ratio: $Heterozygosity_ratio";
			#
			# close the filehandle and exit
			#
			close INCB;
			#
			#
			# Determine Sex
			#
			if ($Heterozygosity_ratio >= 1)
			{
			#
			print "\nSample's Gender is: Female";
			#
			####Female Sample Processing
			#
			# Sort the report FIle on ChrID and Position
			#
			`grep -v 'X' $OutDir/$SampleID/$SampleID.dbsnp147_plus_Informative_GT.txt | grep -v 'Y' | grep -v 'MT' > $OutDir/$SampleID/$SampleID.VCF_and_gVCF_to_dbsnp147_plus_SNP_INDEL_GT_Report.txt`;
			`grep 'X' $OutDir/$SampleID/$SampleID.dbsnp147_plus_Informative_GT.txt >> $OutDir/$SampleID/$SampleID.VCF_and_gVCF_to_dbsnp147_plus_SNP_INDEL_GT_Report.txt`;
			`grep 'MT' $OutDir/$SampleID/$SampleID.dbsnp147_plus_Informative_GT.txt >> $OutDir/$SampleID/$SampleID.VCF_and_gVCF_to_dbsnp147_plus_SNP_INDEL_GT_Report.txt`;
			#
			#
			system("awk '{ if ((length(\$4) <= 3 ) ) print; }' $SampleID.VCF_and_gVCF_to_dbsnp147_plus_SNP_INDEL_GT_Report.txt > $SampleID.VCF_and_gVCF_to_dbsnp147_plus_SNP_GT_PreReport.txt");
			system("sed 's|/||g' $SampleID.VCF_and_gVCF_to_dbsnp147_plus_SNP_GT_PreReport.txt > $SampleID.VCF_and_gVCF_to_dbsnp147_plus_SNP_GT_Report.txt");
			system("awk 'FNR==NR {a[\$1]; next}; \$1 in a' $MyPath/MGB_450Marker.txt $OutDir/$SampleID/$SampleID.VCF_and_gVCF_to_dbsnp147_plus_SNP_GT_Report.txt > $SampleID.VCF_and_gVCF_to_dbsnp147_plus.MGB_450Marker.txt");
			`gzip $OutDir/$SampleID/$SampleID.VCF_and_gVCF_to_dbsnp147_plus_SNP_GT_Report.txt`;
			
			`rm -f $OutDir/$SampleID/$SampleID.dbsnp147_plus_Informative_GT.txt`;
			`rm -f $OutDir/$SampleID/$SampleID.VCF_and_gVCF_to_dbsnp147_plus_SNP_INDEL_GT_Report.txt`;
			`rm -f $OutDir/$SampleID/$SampleID.VCF_and_gVCF_to_dbsnp147_plus_SNP_GT_PreReport.txt`;
			print "\nFinal Report Made at: ";
			print `date`;
			print "\n";
			#
			#############################################
			}
			else
			{
			#
			print "\nSample's Gender is: Male";
			#
			##### Male Sample Processing
			#
			# Sort the report FIle on ChrID and Position
			#
			`grep -v 'X' $OutDir/$SampleID/$SampleID.dbsnp147_plus_Informative_GT.txt | grep -v 'Y' | grep -v 'MT' > $OutDir/$SampleID/$SampleID.VCF_and_gVCF_to_dbsnp147_plus_SNP_INDEL_GT_Report.txt`;
			`grep 'X' $OutDir/$SampleID/$SampleID.dbsnp147_plus_Informative_GT.txt >> $OutDir/$SampleID/$SampleID.VCF_and_gVCF_to_dbsnp147_plus_SNP_INDEL_GT_Report.txt`;
			`grep 'Y' $OutDir/$SampleID/$SampleID.dbsnp147_plus_Informative_GT.txt >> $OutDir/$SampleID/$SampleID.VCF_and_gVCF_to_dbsnp147_plus_SNP_INDEL_GT_Report.txt`;
			`grep 'MT' $OutDir/$SampleID/$SampleID.dbsnp147_plus_Informative_GT.txt >> $OutDir/$SampleID/$SampleID.VCF_and_gVCF_to_dbsnp147_plus_SNP_INDEL_GT_Report.txt`;
			#
			#
			system("awk '{ if ((length(\$4) <= 3 ) ) print; }' $SampleID.VCF_and_gVCF_to_dbsnp147_plus_SNP_INDEL_GT_Report.txt > $SampleID.VCF_and_gVCF_to_dbsnp147_plus_SNP_GT_PreReport.txt");
			system("sed 's|/||g' $SampleID.VCF_and_gVCF_to_dbsnp147_plus_SNP_GT_PreReport.txt > $SampleID.VCF_and_gVCF_to_dbsnp147_plus_SNP_GT_Report.txt");
			system("awk 'FNR==NR {a[\$1]; next}; \$1 in a' $MyPath/MGB_450Marker.txt $OutDir/$SampleID/$SampleID.VCF_and_gVCF_to_dbsnp147_plus_SNP_GT_Report.txt > $SampleID.VCF_and_gVCF_to_dbsnp147_plus.MGB_450Marker.txt");
			`gzip $OutDir/$SampleID/$SampleID.VCF_and_gVCF_to_dbsnp147_plus_SNP_GT_Report.txt`;
			
			`rm -f $OutDir/$SampleID/$SampleID.dbsnp147_plus_Informative_GT.txt`;
			`rm -f $OutDir/$SampleID/$SampleID.VCF_and_gVCF_to_dbsnp147_plus_SNP_INDEL_GT_Report.txt`;
			`rm -f $OutDir/$SampleID/$SampleID.VCF_and_gVCF_to_dbsnp147_plus_SNP_GT_PreReport.txt`;

			print "\nFinal Report Made at: ";
			print `date`;
			print "\n";
			}
			#
			###################################################################################################################
			}	
					else
					{
					print "\nERROR: Check Input Arguments \n\n";
					print "\nVariant File to Genotyping Script: This script is developed to convert the variants in the VCF/gVCF (or both) Files to the 23andMe like Genotyping Table.";
					print "\nUsage Examples:";
					print "\nperl /Path_to_script/Variants-to-dbsnp147-GT_v3.pl SampleID.vcf SampleID.gvcf SampleID path_to_OutputDir";
					print "\nOR";
					print "\nperl /Path_to_script/Variants-to-dbsnp147-GT_v3.pl SampleID.gvcf SampleID path_to_OutputDir";
					print "\nOR";
					print "\nperl /Path_to_script/Variants-to-dbsnp147-GT_v3.pl SampleID.vcf SampleID path_to_OutputDir\n\n";
					exit;
					}
}
#############################################################################################################
#############################################################################################################	